import os
import readline
import rlcompleter
import atexit


# tab completion
readline.parse_and_bind('tab: complete')


# move history file out of home directory
def write_history():
    try:
        readline.write_history_file(history)
    except OSError:
        pass

history = os.path.join(os.getenv("HOME"), '.cache/python_history')
try:
    readline.read_history_file(history)
except OSError:
    pass

atexit.register(write_history)



# import wat to get info about objects with
try:
    from wat import wat
except ImportError:
    pass

print("Welcome, Marcin!")
