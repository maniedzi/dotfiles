# Marcin's shell config
# 
# Custom variables, used to configure stuff below
MY_EDITOR=vim
ALIAS_LOCATION=$HOME/.config/shell/aliases
SECRETS_LOCATION=$HOME/.config/shell/secrets


####################################################################################

# disable less in git diff
export GIT_PAGER=cat git diff
export GIT_EDITOR=$MY_EDITOR

# short warrnings in terraform
export TF_CLI_ARGS_plan="-compact-warnings"
export TF_CLI_ARGS_apply="-compact-warnings"

# editor
export VIMINIT='source $MYVIMRC'; export MYVIMRC=$HOME/.config/vim/vimrc
export EDITOR=$MY_EDITOR
export SELECTED_EDITOR=${EDITOR}

# vault config
export VAULT_ADDR="https://vault.service/"
export VAULT_NAMESPACE="mynamespace"
export VAULT_SKIP_VERIFY=true

# use batcat as manpager 
export MANPAGER="sh -c 'col -bx | batcat -l man -p'" 

# Alias definitions.
if [ -f $ALIAS_LOCATION ]; then
     . $ALIAS_LOCATION
fi

# All my tokens are vars
if [ -f $SECRETS_LOCATION ]; then
    . $SECRETS_LOCATION
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
  PATH="$HOME/.local/bin:$PATH"
fi

# set zathura default pdf viewer on Linux
if [ -f "/usr/bin/xdg-mime" ]; then
  xdg-mime default org.pwmt.zathura.desktop application/pdf
fi

# Cleanup the $HOME directory, and create file where they suppose to be
export LESSHISTFILE=$HOME/.cache/less_history
export PYTHON_HISTORY=$HOME/.cache/python_history
export ZSH_COMPDUMP="${XDG_CACHE_HOME:-$HOME/.cache}/zcompdump-${ZSH_VERSION}"
export HISTFILE="$HOME/.cache/zsh_history"

# python init script
export PYTHONSTARTUP=$HOME/.config/python/pythonstartup.py

# spaceship-prompt
source $HOME/.config/shell/spaceship-prompt/spaceship.zsh
SPACESHIP_DIR_TRUNC_REPO=false
SPACESHIP_TIME_SHOW=false
SPACESHIP_PROMPT_ADD_NEWLINE=true
SPACESHIP_PROMPT_SEPARATE_LINE=true

