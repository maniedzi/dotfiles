# zsh configuration

## Setup

```
sudo apt install zsh git -y

sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

mkdir -p $HOME/.config/shell

git clone https://github.com/spaceship-prompt/spaceship-prompt.git "$HOME/.config/shell/spaceship-prompt" --depth=1

echo "source $HOME/.config/shell/marcin_config.sh" >> $HOME/.zshrc
```

`$HOME/.zshrc`:

```sh
plugins=(
    zsh-syntax-highlighting 
    zsh-autosuggestions 
    fzf-zsh-plugin 
)
```
