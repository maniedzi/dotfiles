# ViM configuration

## Installation

```
mkdir -p $HOME/.config/vim/{backup,tmp,bundle}

git clone https://github.com/VundleVim/Vundle.vim.git $HOME/.config/vim/bundle/Vundle.vim

echo "export VIMINIT='source \$MYVIMRC'; export MYVIMRC=$HOME/.config/vim/vimrc" >> $HOME/.bashrc

cp vimrc $HOME/.config/vim/
```


